# Graph algorithms

Implementation of DFS and Kahn algorithms for graphs represented by an adjacency matrix and a graph matrix in JavaScript.
