let graphEx = {};

graphEx.displayMatrices = (...matrices) => {
  let output = '';
  let style = 2;
  if (style === 1) output += '[\n';
  // matrices[0] - ilość podtablic w pierwszej macierzy (ilość wierszy)
  for (let y=0;y<matrices[0].length;++y){
    matrices.forEach((matrix, ix) => {
      output += '[';
      // matrix[0].length - ilość elementów w pierwszym wierszu macierzy
      for (let x=0;x<matrix[0].length;++x){
        output += matrix[y][x];
        if (style > 1 && x < matrix[0].length-1) output += ',';
      }
      output += ']';
      if (ix < matrices.length-1) output += ' | ';
    });
    if (y < matrices[0].length-1) output += '\n';
  }
  if (style === 1) output += ']';
  console.log(output);
}
graphEx.getVertices = (connections) => {
   let vertices = [];
   connections.forEach((connection) => {
    if (vertices.indexOf(connection[0]) === -1)
      vertices.push(connection[0]);
    if (vertices.indexOf(connection[1]) === -1)
      vertices.push(connection[1]);
  });
  return vertices;
}
graphEx.createConnections = (numberOfVertices, getAlsoVertices=false) => {
  let vertices = [];
  let connections = [];
  for(let i=numberOfVertices;i>0;--i){
    vertices.forEach((v) => connections.unshift([i, v]));
    vertices.unshift(i);
  }
  if (!getAlsoVertices) return connections;
  return {connections: connections, vertices: vertices};
}
graphEx.createAdjMatrix = (connections, vertices, directed=true) => {
  let matrix = new Array(vertices.length);
  for (let i=0;i<vertices.length;++i){
    matrix[i] = new Array(vertices.length).fill(0);
  }
  connections.forEach((connection) => {
    matrix[vertices.indexOf(connection[0])][vertices.indexOf(connection[1])] = 1;
    if (!directed)
      matrix[vertices.indexOf(connection[1])][vertices.indexOf(connection[0])] = 1;
  });
  return matrix;
}
graphEx.getSPnCfromAdjMatrix = (matrix, vertices) => {
  let successors = vertices.map((v, index) => graphEx.getSuccessors(index, matrix, vertices));
  let predecessors = vertices.map((v, index) => graphEx.getPredecessors(index, matrix, vertices));
  let notConnected = vertices.map((v, index) => graphEx.getNotConnected(index, matrix));
  return  {successors: successors, predecessors: predecessors, notConnected: notConnected};
}
graphEx.createGraphMatrix = (connections, vertices, successors, predecessors, notConnected, indexing=0) => {
  let matrix = new Array(vertices.length);
  for (let i=0;i<vertices.length;++i){
    matrix[i] = new Array(vertices.length+3).fill(false);
    matrix[i][vertices.length] = successors[i][0]+indexing;
    matrix[i][vertices.length+1] = predecessors[i][0]+indexing;
    matrix[i][vertices.length+2] = notConnected[i][0]+indexing;
  }
  connections.forEach((connection) => {
    let vi = vertices.indexOf(connection[0]);
    let vj = vertices.indexOf(connection[1]);
    matrix[vi][vj] = successors[vi][successors[vi].length-1]+indexing;
    matrix[vj][vi] = predecessors[vj][predecessors[vj].length-1]+indexing + vertices.length;
  });
  for (let y=0;y<matrix.length;++y){
    for (let x=0;x<matrix[0].length;++x){
      if (matrix[y][x] === false) matrix[y][x] = (notConnected[y][notConnected[y].length-1]+indexing)*(-1);
    }
  }
  return matrix;
}
graphEx.getPredecessors = (x, matrix, vertices, desiredColor='') => {
  let predecessors = [];
  for (let y=0;y<matrix.length;++y){
    if (desiredColor === ''){
      if(matrix[y][x] === 1) predecessors.push(y);
      continue;
    }
    if (matrix[y][x] === 1 && vertices[x].color === desiredColor) predecessors.push(y);
  }
  if (!predecessors.length) return ['-'];
  return predecessors;
}
graphEx.getSuccessors = (y, matrix, vertices, desiredColor='') => {
  let successors = [];
  for (let x=0;x<matrix[y].length;++x){
    if (desiredColor === ''){
      if (matrix[y][x] === 1) successors.push(x);
      continue;
    }
    if (matrix[y][x] === 1 && vertices[x].color === 'gray') return 'cycle'; // There's a cycle among us!
    if (matrix[y][x] === 1 && vertices[x].color === desiredColor) successors.push(x);
  }
  if (!successors.length) return ['-'];
  return successors;
}

graphEx.getNotConnected = (v, matrix) => {
  let notConnected = [];
  for (let x=0;x<matrix[v].length;++x){
    if (matrix[v][x] === 0 && matrix[x][v] === 0) notConnected.push(x);
  }
  return notConnected;
}
graphEx.removeVertex = (v, matrix) => {
  for (let x=0;x<matrix[v].length;++x){
    matrix[v][x] = 0;
  }
  for (let y=0;y<matrix.length;++y){
    matrix[y][v] = 0;
  }
}
graphEx.topologicalSortDfsOnAdjMatrix = (matrix, verticesOriginal) => {
  let vertices = [].concat(verticesOriginal).map((v) =>
    {return {value: v, color: 'white', index: verticesOriginal.indexOf(v)}});;
  let path = [];
  let valPath = [];
  let L = [];
  let i = 0;
  while (true){
    vertices[i].color = 'gray';
    path.push(i);
    valPath.push(vertices[i].value);
    let next = graphEx.getSuccessors(vertices[i].index,matrix, vertices, 'white');
    if (next === 'cycle') return 'Graf zawiera cykl. Sortowanie niemożliwe.';
    if (next[0] !== '-') {
      i = next[0];
      continue;
    }
    else {
      vertices[i].color = 'black';
      L.unshift(vertices[i].value);
    }
    let goBack;
    path.pop();
    while ((goBack = path.pop()) !== undefined){
      if (vertices[goBack].color === 'gray') {
        i = goBack; goBack=true; break;
      }
    }
    if (goBack) continue;
    if (L.length === vertices.length){
      return L;
    }
    for (let x=0;x<vertices.length;++x){
      if (vertices[x].color === 'white') {
        i = vertices[x].index;
        break;
      }
    }
  }
}
graphEx.topologicalSortDfsOnGraphMatrix = (matrix, verticesOriginal, successors) => {
  let vertices = [].concat(verticesOriginal).map((v) =>
    {return {value: v, color: 'white', index: verticesOriginal.indexOf(v)}});;
  let path = [];
  let valPath = [];
  let L = [];
  let i = 0;
  while (true){
    vertices[i].color = 'gray';
    path.push(i);
    valPath.push(vertices[i].value);
    let next = false;
    if (successors[i][0] !== '-')
    for (let x=0;x<successors[i].length;++x){
      if (vertices[successors[i][x]].color === 'gray')
        return 'Graf zawiera cykl. Sortowanie niemożliwe.';
      if (vertices[successors[i][x]].color === 'white'){
        i = successors[i][x];
        next = true;
        break;
      }
    }
    if (next) continue;

    vertices[i].color = 'black';
    L.unshift(vertices[i].value);

    let goBack;
    path.pop();
    while ((goBack = path.pop()) !== undefined){
      if (vertices[goBack].color === 'gray') {
        i = goBack; goBack=true; break;
      }
    }
    if (goBack) continue;
    if (L.length === vertices.length){
      return L;
    }
    for (let x=0;x<vertices.length;++x){
      if (vertices[x].color === 'white') {
        i = vertices[x].index;
        break;
      }
    }
  }
}

graphEx.topologicalSortKahnOnAdjMatrix = (matrixOriginal, verticesOriginal) => {
  let matrix = matrixOriginal.map((row)=>row.slice());
  let vertices = [].concat(verticesOriginal).map((v) =>
    {return {value: v, index: verticesOriginal.indexOf(v)}});;
  let L = [];
  let found;
  let existCount = vertices.length;
  let exists = new Array(vertices.length).fill(true);
  while (existCount){
    found = false;
    for(let x=0;x<vertices.length;++x){
      if (!exists[x]) continue;
      let pre = graphEx.getPredecessors(vertices[x].index, matrix, vertices);
      if (pre[0] === '-'){
        L.push(vertices[x].value);
        graphEx.removeVertex(x, matrix);
        exists[x] = false; existCount--;
        found = true;
        break;
      }
    }
    if (!found) return 'Graf zawiera cykl. Sortowanie niemożliwe.'
  }
  return L;
}
graphEx.topologicalSortKahnOnGraphMatrix = (matrixOriginal, verticesOriginal, predecessorsOriginal) => {
  let matrix = matrixOriginal.map((row)=>row.slice());
  let vertices = [].concat(verticesOriginal).map((v) =>
    {return {value: v, index: verticesOriginal.indexOf(v)}});;
  let predecessors = predecessorsOriginal.map((row)=>{
    if (row[0] === '-') return [];
    return row.slice();
  });
  let L = [];
  let found;
  let existCount = vertices.length;
  let exists = new Array(vertices.length).fill(true);
  while (existCount){
    found = false;
    for(let x=0;x<vertices.length;++x){
      if (!exists[x]) continue;
      if (!predecessors[x].length){
        L.push(vertices[x].value);
        //graphEx.removeVertex(x, matrix);
        // usuniecie wierzcholka - usunięcie go z list poprzedników
        for (let i=0;i<predecessors.length;++i){
          let pre = predecessors[i].indexOf(x);
          if (pre === -1) continue;
          predecessors[i][pre] = predecessors[i][predecessors[i].length-1];
          predecessors[i].pop();
        }
        exists[x] = false; existCount--;
        found = true;
        break;
      }
    }
    if (!found) return 'Graf zawiera cykl. Sortowanie niemożliwe.'
  }
  return L;
}

/*graphEx.DFSonAdjMatrix = (matrix, verticesOriginal) => {
  let vertices = [].concat(verticesOriginal).map((v) =>
    {return {value: v, visited: false, index: verticesOriginal.indexOf(v)}});;
  let stack = [];
  let output = [];
  let i = 0;
  let skipVisiting = false;
  while (true){
    if (!skipVisiting){
      vertices[i].visited = true;
      stack.unshift(vertices[i]);
      output.unshift(vertices[i].value);
    }
    skipVisiting = false;
    let next = graphEx.getSuccessors(vertices[i].index,matrix, vertices, 'white')[0];
    if (next) {
      i = next;
      continue;
    }
    let allVisited = true;
    for (let x=0;x<vertices.length;++x){
      if (!vertices[x].visited){
        allVisited = false; break;
      }
    }
    if (allVisited) return output;
    stack.shift();
    if (stack.length){
      i = stack[0].index;
      continue;
    }
    for (let x=0;x<vertices.length;++x){
      if (!vertices[x].visited) i = vertices[x].index;
      skipVisiting = true;
    }
  }
}
*/

graphEx.validate = () => {
  return 'graph.js is functional';
}

module.exports = graphEx;