const graph = require('graph');
const validate = require('validate');
const Menu = require('menu');
const prompt = require('prompt-sync')();

const fs = require('fs');
const { createGraphMatrix } = require('graph');
const execSync = require('child_process').execSync;

/////////          -- VARIABLES --          /////////
let userData = [];      // connections from the user / generator / file
let tempUserData = false;
let matrix1 = [];
let matrix2 = [];
let vertices = [];

let settings = {
  displayStyle: 2
};

/////////          -- MENU FUNCTIONS --          /////////
function showData(){
  let text = '';
  userData.forEach((data, ix) => {
    text += `[${data}]`;
    if (ix < userData.length-1) text += `, `;
  })
  if (userData.length < 20) {
    console.log(`| Połączenia: [${text}]`);
  }
  else console.log(`| Połączenia: [... ${userData.length} elementów ...]`);
}

let matrixMenuFunctions = [
  () => 'exitMenu',
  () => {
    if (!userData.length){
      console.clear();
      console.log('Aby utworzyć macierze proszę najpierw wprowadzić dane!');
      console.log('Wpisz \'teraz\', aby przejść do menu wprowadzania dancyh');
      console.log('Wciśnij ENTER, aby kontynuować...');
      if (prompt('> ') === 'teraz') mainMenuFunctions[1]();
      return;
    }
    console.clear();
    vertices = graph.getVertices(userData);
    matrix1 = graph.createAdjMatrix(userData, vertices);
    let {successors: successors, predecessors: predecessors, notConnected: notConnected} = graph.getSPnCfromAdjMatrix(matrix1, vertices);
    matrix2 = graph.createGraphMatrix(userData, vertices, successors, predecessors, notConnected);
    console.log('Pomyślnie wygenerowano macierze');
    console.log(`Wierzchołki: ${vertices}`);
    graph.displayMatrices(matrix1, matrix2);
    prompt();
    return;
  },
  () => {
    if (! matrix1.length){
      console.clear();
      console.log('Aby wykonywać operacje na macierzach proszę najpierw je utworzyć!');
      console.log('Wpisz \'teraz\', aby od razu utworzyć macierze');
      console.log('Wciśnij ENTER, aby anulować...');
      if (prompt('> ') === 'teraz') matrixMenuFunctions[1]();
      return;
    }
    console.clear();
    console.log('-- SORTOWANIE TOPOLOGICZNE - DFS --');
    let output = graph.topologicalSortDfsOnAdjMatrix(matrix1, vertices);
    console.log(`Wynik dla reprezentacji wykorzystującej macierz sąsiedztwa:\n${output}`);
    let {successors: successors/*, predecessors: predecessors, notConnected: notConnected*/} = graph.getSPnCfromAdjMatrix(matrix1, vertices);
    let output2 = graph.topologicalSortDfsOnGraphMatrix(matrix2, vertices, successors);
    console.log(`Wynik dla reprezentacji wykorzystującej macierz grafu:\n${output2}`);
    prompt();
  },
  () => {
    if (! matrix1.length){
      console.clear();
      console.log('Aby wykonywać operacje na macierzach proszę najpierw je utworzyć!');
      console.log('Wpisz \'teraz\', aby od razu utworzyć macierze');
      console.log('Wciśnij ENTER, aby anulować...');
      if (prompt('> ') === 'teraz') matrixMenuFunctions[1]();
      return;
    }
    console.clear();
    console.log('-- SORTOWANIE TOPOLOGICZNE - ALGORYTM KAHNA --');
    let output = graph.topologicalSortKahnOnAdjMatrix(matrix1, vertices);
    console.log(`Wynik dla reprezentacji wykorzystującej macierz sąsiedztwa:\n${output}`);
    let {/*successors: successors, */predecessors: predecessors/*, notConnected: notConnected*/} = graph.getSPnCfromAdjMatrix(matrix1, vertices);
    let output2 = graph.topologicalSortKahnOnGraphMatrix(matrix2, vertices, predecessors);
    console.log(`Wynik dla reprezentacji wykorzystującej macierz grafu:\n${output2}`);
    prompt();
  },
  () => {
    if (! matrix1.length){
      console.clear();
      console.log('Aby wykonywać operacje na macierzach proszę najpierw je utworzyć!');
      console.log('Wpisz \'teraz\', aby od razu utworzyć macierze');
      console.log('Wciśnij ENTER, aby anulować...');
      if (prompt('> ') === 'teraz') matrixMenuFunctions[1]();
      return;
    }
    console.clear();
    console.log('-- PODGLĄD MACIERZY --');
    console.log(`Wierzchołki: ${vertices}`);
    graph.displayMatrices(matrix1, matrix2);
    prompt();
  },
  () => {
    if (!userData.length){
      console.clear();
      console.log('Aby zwizualzować graf proszę najpierw wprowadzić dane!');
      console.log('Wpisz \'teraz\', aby przejść do menu wprowadzania dancyh');
      console.log('Wciśnij ENTER, aby kontynuować...');
      if (prompt('> ') === 'teraz') mainMenuFunctions[1]();
      return;
    }
    console.clear();
    console.log('-- WIZUALIZACJA --');
    let visualPath = __dirname + '/visual';

    let tempFileName = 'graph.dot';
    let tempFilePath = `${visualPath}/temp/${tempFileName}`;

    let textContent = 'digraph {\n';
    userData.forEach((connection) => {
      textContent += `\t${connection[0]} -> ${connection[1]};\n`
    })
    textContent += '}';
    try {
      fs.writeFileSync(tempFilePath, textContent, {encoding: 'utf-8'});
      console.log('...successfully written to ${tempFileName} file');
    } catch (err){
      console.log(err);
    }
    //Drawing
    let outputName = 'graph.png';
    let outputPath = `${visualPath}/${outputName}`;
    let program = 'dot';
    let options = `-Tpng -o ${outputPath}`;
    try {
      execSync(`${program} ${options} ${tempFilePath}`);
      console.log(`...successfully drawn the graph to ${outputName} using ${program}`);
    } catch (err){
      console.log(err);
    }
    prompt();
  }
];
let matrixMenuTexts = ['Utwórz macierze', 'Sortowanie topologiczne - DFS', 'Sortowanie topologiczne - Algorytm Kahna', 'Wyświetl macierze', 'Zwizualizuj grafy', 'Powrót'];
let matrixMenu = new Menu(matrixMenuFunctions, matrixMenuTexts, {
  title: '-- OPERACJE NA GRAFIE --',
  extraInfo: () => {
    if (vertices.length) console.log(`Wierzchołki: ${vertices}`);
    else console.log('Nie utworzono macierzy do sortowania topologicznego!');
  },
  subtitle: 'Wprowadź numer operacji i zatwierź przyciskiem ENTER:',
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});

let dataInputMenuFunctions = [
  () => 'exitMenu',
  () => {
    let style = 0;
    while(true){
    console.clear();
    console.log('-- WPROWADZANIE DANYCH --');
    showData();
    console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
    console.log(`Podaj element:`);
    if (style === 1 ) console.log(`Podaj prawidłowy element!`);
    style = 0;
    option = prompt('> ')
    if (option === '') {
      if (tempUserData === true) userData.pop();
      tempUserData = false;
      break;
    }
    if ((option = validate.strToInt(option)) === false){
      style = 1; continue;
    }
    if (tempUserData === false){
      tempUserData = true;
      userData.push([option, '-']);
      continue;
    }
    userData[userData.length-1][1] = option;
    tempUserData = false;
  }},
  () => userData.pop(),
  () => {userData = [];}
];
let dataInputMenuTexts = ['Wprowadź dane', 'Usuń ostatni element', 'Wyczyść', 'Powrót'];
let dataInputMenu = new Menu(dataInputMenuFunctions, dataInputMenuTexts, {
  title: '-- WPROWADZANIE DANYCH --',
  extraInfo: () => {
    showData()
  },
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});
let dataMenuFunctions = [
  () => 'exitMenu',
  () => dataInputMenu.loop(),
  () => {
    let option = '0';
    let style = 0;
    while(true){
      console.clear();
      console.log('-- GENEROWANIE GRAFU --');
      console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
      console.log(`Podaj ilość wierzchołków w wyjściowym grafie (n):`);
      if (style > 0 ) console.log(`Podaj prawidłową wartość!`);
      style = 0;
      option = prompt('> ');
      if (option === '') return;
      if (!(option = validate.strToInt(option))){
        style = 1; continue;
      }
      userData = graph.createConnections(option, false);
      console.log(userData);
      prompt();
      return;
    }
  },
  () => {
    let path;
    let file;
    while(true){
      console.clear();
      console.log('-- WCZYTYWANIE GRAFU Z PLIKU --');
      console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
      console.log(`Proszę podać ścieżkę do pliku:`);
      path = prompt('> ');
      if (path === '') return;
      try {
        file = fs.readFileSync(path, { encoding: 'utf8' });
        break;
      } catch(err) {
        console.log('Błąd wprawadzania!');
        console.log('\t' + err.message);
        console.log('\nWciśnij ENTER, aby kontynuować...');
        prompt();
      }
    }
    file = file.split('\n').map((line) => line.split(' '));
    let lineOne = file[0];
    let vertices = validate.strToInt(lineOne[0]);
    let edges = validate.strToInt(lineOne[1]);
    if (vertices === false || edges === false || vertices<1 || edges < 1){
      console.clear();
      console.log(`Plik zawiera nieprawidłowe dane`);
      prompt();
      return;
    }
    let v1, v2;
    let input = [];
    for (let x=0;x<edges;++x){
      v1 = validate.strToInt(file[x+1][0]);
      v2 = validate.strToInt(file[x+1][1]);
      if (vertices === false || edges === false){
        console.clear();
        console.log(`Plik zawiera nieprawidłowe dane`);
        prompt();
        return;
      }
      input.push([v1, v2]);
    }
    let verts = graph.getVertices(input);
    userData = input;
    if (verts.length !== vertices){
      console.clear();
      console.log(`Podana ilość wierzchołków nie jest zgodna z podanymi połączeniami`);
      console.log(`Ten błąd jest jednak nieznaczny i graf wczytano zakładając poprawność połączeń`);
      console.log('Wciśnij ENTER, aby kontynuować...');
      prompt();
      return;
    }
    console.clear();
    console.log(`Pomyślnie wczytano dane`);
    console.log('Wciśnij ENTER, aby kontynuować...');
    prompt();
  }
];
let dataMenuTexts = ['Wprowadź dane z klawiatury', 'Pobierz dane z generatora', 'Pobierz dane z pliku', 'Powrót'];
let dataMenu = new Menu(dataMenuFunctions, dataMenuTexts, {
  title: '-- OPERACJE NA DANYCH --',
  /*extraInfo: () => {
    showData()
  },*/
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});
let mainMenuFunctions = [
  () => 'exitMenu',
  () => dataMenu.loop(),
  () => matrixMenu.loop(),
  () => {
    let option = '0';
    let style = 0;
    while(true){
      console.clear();
      console.log('-- USTAWIENIA --');
      console.log(`Nie ma jeszcze żadnych ustawień`);
      prompt();
    }
  },
  () => {
    console.clear();
    console.log('-- TESTOWANIE SORTOWANIA TOPOLOGICZNEGO --');
    /* Wygeneruj n-elementowe posortowane malejąco ciągi liczb naturalnych (dla 10-15 różnych wartości n z przedziału <10,k>,
      przy czym k należy dobrać eksperymentalnie tak, aby możliwe było wykonanie pomiarów i aby jego wartość była możliwie duża).
    */
    let ns = [];
    let currentValue = 100;
    let krok = 70;
    for (let x = 0; x<14; ++x){
      ns.push(currentValue);
      currentValue+=krok;
    }
    let NUMBER = 10;

    testData = [];
    let t1, t2;
    let testTimes = '';
    ns.forEach((n, ix) => {
        for(let x=0;x<NUMBER;++x){
          console.log(`[${ix}:${x}/${ns.length}] Tworzenie testu dla n=${n}...`);
          let {connections: connections, vertices: vertices} = graph.createConnections(n, true);
          t1 = performance.now();
          let adjMatrix = graph.createAdjMatrix(connections, vertices);
          t2 = performance.now();
          let time1 = Math.floor((t2 - t1)*1000)/1000;
          t1 = performance.now();
          let {successors: successors, predecessors: predecessors, notConnected: notConnected}
            = graph.getSPnCfromAdjMatrix(adjMatrix, vertices);
          let graphMatrix = graph.createGraphMatrix(connections, vertices, successors, predecessors, notConnected);
          t2 = performance.now();
          let time2 = Math.floor((t2 - t1)*1000)/1000;
          console.log(`${n};${time1};${time2}`);
          testTimes += `${n};${time1};${time2}\n`;
          testData.push(
            {connections: connections,
              vertices: vertices,
              adjMatrix: adjMatrix,
              graphMatrix: graphMatrix,
              successors: successors,
              predecessors: predecessors
            });
        }
      });
    console.log('TEST OUTPUT');
    console.log('___________');
    console.log('Matrix generation duration:');
    console.log(testTimes);

    console.log('___________');
    console.log('Topological sorting duration:');
    let test;
    for(let i=0; i<testData.length; ++i){
      test = testData[i];
      let {time: adjDfsTime}
        = measure(graph.topologicalSortDfsOnAdjMatrix, test.adjMatrix, test.vertices);
      let {time: adjKahnTime}
        = measure(graph.topologicalSortKahnOnAdjMatrix, test.adjMatrix, test.vertices);
      let {time: graphDfsTime}
        = measure(graph.topologicalSortDfsOnGraphMatrix, test.graphMatrix, test.vertices, test.successors);
      let {time: graphKahnTime}
        = measure(graph.topologicalSortKahnOnGraphMatrix, test.adjMatrix, test.vertices, test.predecessors);

      console.log(`${test.vertices.length};${adjDfsTime};${adjKahnTime};${graphDfsTime};${graphKahnTime}`);
    }
    console.log('Wciśnij ENTER, aby zakończyć...');
    prompt();
  }
];

let mainMenuTexts = ['Wprowadź dane', 'Operacje na grafie', 'Ustawienia', 'Testy', 'Wyjdź'];
let mainMenu = new Menu(mainMenuFunctions, mainMenuTexts, {
  title: '-- MENU --',
  inputError: 'Podaj prawidłowy numer operacji!',
  getInput: prompt
});

function measure(fun, ...args){
  let t1 = performance.now();
  const out = fun(...args);
  let t2 = performance.now();
  let time = Math.floor((t2 - t1)*1000)/1000;
  return {out: out, time: time}
}

//userData = [[1,2],[1,10],[2,3],[2,7],[3,4],[4,6],[6,9],[8,4],[8,9],[8,6],[8,9],[7,9],[7,6],[7,8],[5,7],[5,2],[10,5],[10,6]];
//userData = [[1,2],[3,1],[3,2],[2,4],[2,5],[4,3],[5,1],[5,4]];

function debug(){
  vertices = graph.getVertices(userData);

  matrix1 = graph.createAdjMatrix(userData, vertices);
  let {successors: successors, predecessors: predecessors, notConnected: notConnected} = graph.getSPnCfromAdjMatrix(matrix1, vertices);
    matrix2 = graph.createGraphMatrix(userData, vertices, successors, predecessors, notConnected);
  let output2 = graph.topologicalSortDfsOnGraphMatrix(matrix2, vertices, successors);
      console.log(`Wynik dla reprezentacji wykorzystującej macierz grafu:\n${output2}`);
  prompt();
}

//userData = [[1,3],[2,3],[3,5],[4,6],[5,7],[6,2]];
//debug();
mainMenu.loop();